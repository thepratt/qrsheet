<?php

require_once 'vendor/autoload.php';

use \PHPQRCode\QRcode;

class Generate {
    private $route;
    private $dir;
    private $codes;

    public function __construct($route, $dir = null) {
        set_time_limit(0);
        $this->route = $route;
        $this->dir = is_null($dir) ? '/images/' : $dir;
    }

    public function execute($min, $max) {
        for ($ii = $min; $ii <= $max; ++$ii) {
            $route = sprintf('%s%s', $this->route, $ii);
            $fileName = sprintf('%s%u.png', $this->dir, $ii);

            try {
                QRcode::png($route, __DIR__ . $fileName, 'S', 4, 2);
                $this->codes[] = $fileName;
            } catch (\Exception $e) {}
        }
    }

    public function getCodes() {
        return $this->codes;
    }
}

$g = new Generate('http://lyte.de/r/');
$g->execute(1, 1000);

foreach ($g->getCodes() as $code):?>
<img src="<?php echo $code; ?>" />
<?php endforeach;
