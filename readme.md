## README

For minimal setup, follow these instructions:  


* run `composer install`
* Edit generate.php for the numbers you wish to loop over
* run: `php -S localhost:1337`
* navigate to URL
* page (with no timeout) will generate all images, then include them on the HTML page
* save page as PDF